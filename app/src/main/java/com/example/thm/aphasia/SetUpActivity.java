package com.example.thm.aphasia;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.example.thm.aphasia.domain.tests.TestManager;
import com.example.thm.aphasia.io.DirSetUp;

/**
 * Created by Jeremy on 1/22/2017.
 */

public class SetUpActivity extends Activity
{
    private boolean done;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_up_layout);
        done = false;
        InitTestsAsync initTestsAsync = new InitTestsAsync();
        Toast.makeText(this, R.string.setup_tests_pictures, Toast.LENGTH_LONG).show();
        initTestsAsync.execute();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("done", this.done).commit();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.done = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("done", false);
        if (done)
            finish();
    }

    private void initTests()
    {
        TestManager.getInstance().InitPictureChooseTests();
        TestManager.getInstance().InitSpeechAntwoordTests();
        TestManager.getInstance().InitScenarioTests();
    }

    @Override
    public void finish()
    {
        if (done)
        {
            Intent i = new Intent();
            setResult(RESULT_OK);
            super.finish();
        }
    }

    private class InitTestsAsync extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... nothing)
        {
            DirSetUp.SetUpDirs(getApplicationContext(), true);
            initTests();
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            done = true;
            finish();
        }
    }
}
