package com.example.thm.aphasia;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.example.thm.aphasia.domain.Result;
import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.tests.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeremy on 1/19/2017.
 */

public abstract class MainTestActivity extends Activity
{
    private int currentQuestion;
    private long resultId;
    private List<Test> testList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    public int getCurrentQuestionIndex()
    {
        return currentQuestion;
    }

    public Result getResult()
    {
        Result result = Result.findById(Result.class, resultId);
        if (result == null)
            return null;

        return result.initTestsResults();
    }

    public Test getCurrentQuestion()
    {
        return testList.get(currentQuestion);
    }

    private void initQuestions()
    {
        if (testList == null || testList.isEmpty())
        {
            testList = new ArrayList<>();
            testList.addAll(Test.find(Test.class, "approved = ?", "1"));
            for (Test test : testList)
                test.initOptions();
        }

        if (currentQuestion == -1)
            nextQuestion();
    }

    public void nextQuestion()
    {
        if (currentQuestion < testList.size() - 1)
        {
            currentQuestion++;
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putInt("currentQuestion", currentQuestion);
            editor.apply();
            showInList();
        }
        else
            cleanUp();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putInt("currentQuestion", currentQuestion);
        editor.putLong("currentResultId", resultId);
        editor.apply();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        currentQuestion = PreferenceManager.getDefaultSharedPreferences(this).getInt("currentQuestion", -1);
        initQuestions();
        resultId = PreferenceManager.getDefaultSharedPreferences(this).getLong("currentResultId", -1);
        if (resultId == -1)
        {
            Result result = new Result();
            resultId = result.save();
            PreferenceManager.getDefaultSharedPreferences(this).edit().putLong("currentResultId", resultId).commit();
        }

        this.showInList();
    }

    private void cleanUp()
    {
        Intent showScore = new Intent(this, ResultAfterTestActivity.class);
        startActivityForResult(showScore, Constants.REQUEST_CODE_SHOW_SCORE_ACTIVITY_RESULT);
    }

    @Override
    public void onDestroy()
    {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.clear();
        editor.apply();
        super.onDestroy();
    }

    @Override
    public void finish()
    {
        Intent i = new Intent();
        if (currentQuestion < testList.size() - 1)
        {
            setResult(RESULT_CANCELED, i);
            Result toDeleteResult = Result.findById(Result.class, resultId);
            toDeleteResult.delete();
        }
        else
            setResult(RESULT_OK, i);

        super.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constants.REQUEST_CODE_SHOW_SCORE_ACTIVITY_RESULT)
            this.finish();
    }

    public void addTestResult(Test test, Answer answer)
    {
        Result result = getResult();
        if (result == null)
            return;

        result.addTestResult(test, answer);
        result.save();
    }

    // Show possible answers
    public void showInList() { }
}
