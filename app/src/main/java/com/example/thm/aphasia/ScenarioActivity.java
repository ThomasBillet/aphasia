package com.example.thm.aphasia;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Picture;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.thm.aphasia.adapters.PictureGridAdapter;
import com.example.thm.aphasia.adapters.ScenarioAdapter;
import com.example.thm.aphasia.domain.Result;
import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.tests.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Thomas on 5/12/2016.
 */

public class ScenarioActivity extends MainTestActivity
{
    private GridView gridView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.picture_to_scenario_layout);
        this.gridView = (GridView) findViewById(R.id.answerlijst);
        this.textView = (TextView) findViewById(R.id.scenarioText);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showInList()
    {
        textView.setText(getCurrentQuestion().getQuestion());
        List<Answer> possibleAnswers = getCurrentQuestion().giveOptions();
        final PictureGridAdapter adapter = new PictureGridAdapter(this, possibleAnswers);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                addTestResult(getCurrentQuestion(), (Answer)adapter.getItem(position));
                nextQuestion();
            }
        });
    }
}





