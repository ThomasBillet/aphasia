package com.example.thm.aphasia;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.tests.Test;
import com.example.thm.aphasia.domain.tests.TestManager;
import com.example.thm.aphasia.domain.tests.TestType;
import com.orm.SugarContext;

import java.util.List;

public class MainActivity extends AppCompatActivity
{
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case R.id.selectpicturechoosetest:
                this.handlePictureChooseTest();
                return true;
            case R.id.selectsayoutlouttest:
                this.handleSayOutLoud();
                return true;
            case R.id.selectscenariotest:
                this.handleScenario();
                return true;
            case R.id.selectresultaat:
                this.handleResultaat();
                return true;
            case R.id.selectquestionloading:
                this.handleLoadQuestions();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleResultaat()
    {
        Intent i = new Intent(this, ResultaatActivity.class);
        startActivityForResult(i, Constants.REQUEST_CODE_RESULTAAT_ACTIVITY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        SugarContext.init(this);
        setContentView(R.layout.activity_main);

        if (Answer.count(Answer.class) <= 0)
        {
            Intent i = new Intent(this, SetUpActivity.class);
            startActivityForResult(i, Constants.REQUEST_CODE_SET_UP_ACTIVITY);
        }

        TestManager.getInstance().SetAllTestApprovedStateToFalseAsync();
    }

    @Override
    protected  void onDestroy()
    {
        super.onDestroy();
        SugarContext.terminate();
    }

    private void handlePictureChooseTest()
    {
        ApproveDbTask task = new ApproveDbTask();
        task.doInBackground(new DbAsyncTaskParams("SELECT * FROM TEST WHERE TYPE = ? ORDER BY RANDOM() LIMIT 5", TestType.TEST_TYPE_PICTURE_CHOOSE.toString()));
        Intent i = new Intent(this, PictureChooseActivity.class);

        startActivityForResult(i, 1);
    }

    private void handleLoadQuestions()
    {
        Intent i = new Intent(this, QuestionLoadActivity.class);

        startActivityForResult(i, Constants.REQUEST_CODE_TEST_ACTIVITY);
    }

    private void handleSayOutLoud()
    {
        ApproveDbTask task = new ApproveDbTask();
        task.doInBackground(new DbAsyncTaskParams("SELECT * FROM TEST WHERE TYPE = ? ORDER BY RANDOM() LIMIT 5", TestType.TEST_TYPE_SAY_OUT_LOUD.toString()));
        Intent i = new Intent(this, SpeechAntwoordActivity.class);

        startActivityForResult(i, Constants.REQUEST_CODE_TEST_ACTIVITY);
    }

    private void handleScenario()
    {
        ApproveDbTask task = new ApproveDbTask();
        task.doInBackground(new DbAsyncTaskParams("SELECT * FROM TEST WHERE TYPE = ? ORDER BY RANDOM() LIMIT 5", TestType.TEST_TYPE_SCENARIO.toString()));
        Intent i = new Intent(this, ScenarioActivity.class);

        startActivityForResult(i, Constants.REQUEST_CODE_TEST_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // cancelled or not, set approved state back to false.
        if (requestCode == Constants.REQUEST_CODE_TEST_ACTIVITY)
        {
            TestManager.getInstance().SetAllTestApprovedStateToFalse();
        }
        else if (requestCode == Constants.REQUEST_CODE_SET_UP_ACTIVITY && resultCode == RESULT_OK)
        {
            Toast.makeText(this, R.string.setup_tests_pictures_done, Toast.LENGTH_SHORT).show();
        }
    }

    private class ApproveDbTask extends AsyncTask<DbAsyncTaskParams, Void,Void>
    {
        @Override
        protected Void doInBackground(DbAsyncTaskParams... query)
        {
            List<Test> tests = Test.findWithQuery(Test.class, query[0].getQuery(), query[0].getType());

            for (Test test : tests)
            {
                test.setApproved(true);
                test.save();
            }

            return null;
        }
    }
}


