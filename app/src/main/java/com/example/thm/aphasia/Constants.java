package com.example.thm.aphasia;

/**
 * Created by Jeremy on 1/24/2017.
 */

public final class Constants
{
    public static final int REQUEST_CODE_SPEECH = 0;
    public static final int REQUEST_CODE_TEST_ACTIVITY = 1;
    public static final int REQUEST_CODE_SHOW_SCORE_ACTIVITY_RESULT = 2;
    public static final int REQUEST_CODE_RESULTAAT_ACTIVITY = 3;
    public static final int REQUEST_CODE_SET_UP_ACTIVITY = 4;

    public static final String ROOT_DIR_NAME = "aphasiadirectory";
    public static final String PICTURES_DIR_NAME = "pictures";
    public static final String PICTURES_EXTENSION = ".png";
    public static final String PICTURES_PATH_FROM_ROOT = "/" + ROOT_DIR_NAME + "/" + PICTURES_DIR_NAME + "/";

    public static final String SCENARIO_QUESTIONS_FILE = "scenarioquestions.txt";
    public static final String PICTURE_CHOOSE_QUESTIONS_FILE = "picturechoosequestions.txt";
    public static final String SPEECH_QUESTIONS_FILE = "speechquestions.txt";
}
