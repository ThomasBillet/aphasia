package com.example.thm.aphasia.io;

import android.content.Context;

import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.answers.AnswerType;
import com.example.thm.aphasia.domain.tests.Test;
import com.example.thm.aphasia.domain.tests.TestType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by Thomas on 26/11/2016.
 */

public class TestReader
{
    private String filename;
    private AnswerType answertype;
    private TestType testType;

    public TestReader(String filename, AnswerType answertype, TestType testType)
    {
        this.filename = filename;
        this.answertype = answertype;
        this.testType = testType;
    }

    public void lees(Context context)
    {
        try
        {
            File root = DirSetUp.SetUpDirs(context, false);

            File file = new File(root,this.filename);
            if (!file.exists())
               file.createNewFile();

            BufferedReader in = new BufferedReader(new FileReader(file));

            while (in.ready())
            {
                String line = in.readLine();
                String[] splits = line.split(":");

                Answer answer = new Answer(splits[0], answertype);
                answer.save();
                Test test = new Test(false, answer, testType, splits[1]);
                test.save();
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
