package com.example.thm.aphasia.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.thm.aphasia.R;
import com.example.thm.aphasia.domain.answers.Answer;

import java.util.List;

/**
 * Created by Thomas on 12/11/2016.
 */

@Deprecated
public class PictureAdapter extends ArrayAdapter<Answer> {


    public PictureAdapter(Context context, int resource) {
        super(context, resource);
    }

    public PictureAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public PictureAdapter(Context context, int resource, Answer[] objects) {
        super(context, resource, objects);
    }

    public PictureAdapter(Context context, int resource, int textViewResourceId, Answer[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public PictureAdapter(Context context, int resource, List<Answer> objects) {
        super(context, resource, objects);
    }

    public PictureAdapter(Context context, int resource, int textViewResourceId, List<Answer> objects) {
        super(context, resource, textViewResourceId, objects);
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.picturelist_item, null);
        }
        //FIX
        Answer p = this.getItem(position);
        if (p != null) {

            ImageView tt1 = (ImageView) v.findViewById(R.id.imageview);

            Resources resources = getContext().getResources();
            final int resId = resources.getIdentifier(p.getAnswer(), "drawable", getContext().getPackageName());
            tt1.setImageDrawable(resources.getDrawable(resId));
            //TODO FIX


        }

        return v;
    }


}
