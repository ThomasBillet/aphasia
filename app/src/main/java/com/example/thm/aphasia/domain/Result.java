package com.example.thm.aphasia.domain;

import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.tests.Test;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Thomas on 13/11/2016.
 */

public class Result extends SugarRecord
{
    // Private class ?
    // SugarORM kan niet omgaan met lists...
    @Ignore
    private List<TestWithAnswer> testsResults;

    private String testResultsJSON;
    private Date date = new Date();

    public Result()
    {
        testsResults = new ArrayList<>();
        testResultsJSON = new Gson().toJson(testsResults);
    }

    // method private maken als de testWithAnser class private is
    public void addTestResult(TestWithAnswer testWithAnswer)
    {

        this.testsResults.add(testWithAnswer);
        testResultsJSON = new Gson().toJson(testsResults);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLength(){

        initTestsResults();
        return this.testsResults.size();
    }

    public void addTestResult(Test test, Answer answer)
    {
        this.addTestResult(new TestWithAnswer(test, answer));
    }

    public void resetResults()
    {
        testsResults.clear();
    }

    public int calculateScore()
    {
        initTestsResults();
        int score = 0;
        for (TestWithAnswer result : testsResults)
            if (result.isCorrect())
                score++;

        return score;
    }

    public String getTestTypeAsString()
    {
        if (testsResults.isEmpty())
            return "ERROR_NO_TESTS_IN_RESULT";

        return testsResults.get(0).getTest().getTestType().getStringRepresentation();
    }

    // Alleen als save opgeroepen is.
    public Result initTestsResults()
    {
        testsResults = new ArrayList<>();
        if (testResultsJSON != null && !testResultsJSON.isEmpty())
        {
            Type type = new TypeToken<List<TestWithAnswer>>(){}.getType();
            testsResults = new Gson().fromJson(testResultsJSON, type);
        }

        return this;
    }

    /// VIEZE CODE... SugarORM kan ni omgaan met lists... dus ipv de list op te slaan, slaan we een json object op.
    @Override
    public long save()
    {
        if (testsResults != null)
            testResultsJSON = new Gson().toJson(testsResults);

        return super.save();
    }
}
