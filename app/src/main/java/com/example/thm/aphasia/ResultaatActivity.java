package com.example.thm.aphasia;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.thm.aphasia.adapters.ResultAdapter;
import com.example.thm.aphasia.domain.Result;

import java.util.List;

/**
 * Created by Thomas on 4/12/2016.
 */

public class ResultaatActivity extends Activity
{
    private ListView lview;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.result_overview_layout);
        this.lview = (ListView) findViewById(R.id.result_listview) ;

        this.initList();
    }

    private void initList()
    {
        List<Result> results = Result.listAll(Result.class);
        for (Result result : results)
            result.initTestsResults();

        ResultAdapter adapter = new ResultAdapter(this, R.layout.result_layout_item_layout, results);
        this.lview.setAdapter(adapter);
    }
}




