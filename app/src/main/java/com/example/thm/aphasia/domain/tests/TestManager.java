package com.example.thm.aphasia.domain.tests;

import android.os.AsyncTask;

import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.answers.AnswerType;

import java.util.List;

/**
 * Created by Jeremy on 1/18/2017.
 */

public final class TestManager
{
    private static final TestManager instance = new TestManager();

    private TestManager() { }

    public static TestManager getInstance()
    {
        return instance;
    }
    /*
     * Creates a Test object with a answer. Saves both in the database & returns the created test.
     * Note: test approved state is set to false.
     */
    public Test createTest(String answerText, AnswerType answerType, String question, TestType testType)
    {
        Answer answer = new Answer(answerText, answerType);
        answer.save();
        Test test = new Test(false, answer, testType, question);
        test.save();

        return test;
    }

    public void InitPictureChooseTests()
    {
        createTest("book", AnswerType.ANSWER_TYPE_IMAGE_URI, "book", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("candle", AnswerType.ANSWER_TYPE_IMAGE_URI, "candle", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("arrow", AnswerType.ANSWER_TYPE_IMAGE_URI, "arrow", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("car", AnswerType.ANSWER_TYPE_IMAGE_URI, "car", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("can", AnswerType.ANSWER_TYPE_IMAGE_URI, "can", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("cat", AnswerType.ANSWER_TYPE_IMAGE_URI, "cat", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("computer", AnswerType.ANSWER_TYPE_IMAGE_URI, "computer", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("cow", AnswerType.ANSWER_TYPE_IMAGE_URI, "cow", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("dog", AnswerType.ANSWER_TYPE_IMAGE_URI, "dog", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("glasses", AnswerType.ANSWER_TYPE_IMAGE_URI, "glasses", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("gorilla", AnswerType.ANSWER_TYPE_IMAGE_URI, "gorilla", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("mouse", AnswerType.ANSWER_TYPE_IMAGE_URI, "mouse", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("pen", AnswerType.ANSWER_TYPE_IMAGE_URI, "pen", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("horse", AnswerType.ANSWER_TYPE_IMAGE_URI, "horse", TestType.TEST_TYPE_PICTURE_CHOOSE);
        createTest("table", AnswerType.ANSWER_TYPE_IMAGE_URI, "table", TestType.TEST_TYPE_PICTURE_CHOOSE);
    }

    public void InitSpeechAntwoordTests()
    {
        createTest("arrow", AnswerType.ANSWER_TYPE_TEXT, "arrow", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("book", AnswerType.ANSWER_TYPE_TEXT, "book", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("can", AnswerType.ANSWER_TYPE_TEXT, "can", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("candle", AnswerType.ANSWER_TYPE_TEXT, "candle", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("car", AnswerType.ANSWER_TYPE_TEXT, "car", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("cat", AnswerType.ANSWER_TYPE_TEXT, "cat", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("computer", AnswerType.ANSWER_TYPE_TEXT, "computer", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("cow", AnswerType.ANSWER_TYPE_TEXT, "cow", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("dog", AnswerType.ANSWER_TYPE_TEXT, "dog", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("glasses", AnswerType.ANSWER_TYPE_TEXT, "glasses", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("gorilla", AnswerType.ANSWER_TYPE_TEXT, "gorilla", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("horse", AnswerType.ANSWER_TYPE_TEXT, "horse", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("keyboard", AnswerType.ANSWER_TYPE_TEXT, "keyboard", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("lamp", AnswerType.ANSWER_TYPE_TEXT, "lamp", TestType.TEST_TYPE_SAY_OUT_LOUD);
        createTest("mouse", AnswerType.ANSWER_TYPE_TEXT, "mouse", TestType.TEST_TYPE_SAY_OUT_LOUD);
    }

    public void InitScenarioTests()
    {
        createTest("arresting", AnswerType.ANSWER_TYPE_IMAGE_URI, "A policeman arresting someone", TestType.TEST_TYPE_SCENARIO);
        createTest("dancing", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone dancing with music on in the background", TestType.TEST_TYPE_SCENARIO);
        createTest("eating", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone eating", TestType.TEST_TYPE_SCENARIO);
        createTest("fighting", AnswerType.ANSWER_TYPE_IMAGE_URI, "Two people having a fight", TestType.TEST_TYPE_SCENARIO);
        createTest("fishing", AnswerType.ANSWER_TYPE_IMAGE_URI, "A guy in a boat fishing", TestType.TEST_TYPE_SCENARIO);
        createTest("guitar", AnswerType.ANSWER_TYPE_IMAGE_URI, "A guy playing guitar", TestType.TEST_TYPE_SCENARIO);
        createTest("playing_footbal", AnswerType.ANSWER_TYPE_IMAGE_URI, "A guy playing football", TestType.TEST_TYPE_SCENARIO);
        createTest("showering", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone taking a shower", TestType.TEST_TYPE_SCENARIO);
        createTest("sleeping", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone sleeping", TestType.TEST_TYPE_SCENARIO);
        createTest("swimming", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone swimming", TestType.TEST_TYPE_SCENARIO);
        createTest("thinking", AnswerType.ANSWER_TYPE_IMAGE_URI, "A guy thinking really hard...", TestType.TEST_TYPE_SCENARIO);
        createTest("watching_tv", AnswerType.ANSWER_TYPE_IMAGE_URI, "Watching tv", TestType.TEST_TYPE_SCENARIO);
        createTest("working", AnswerType.ANSWER_TYPE_IMAGE_URI, "Someone working with a big hammer", TestType.TEST_TYPE_SCENARIO);
        createTest("tennis", AnswerType.ANSWER_TYPE_IMAGE_URI, "Playing tennis", TestType.TEST_TYPE_SCENARIO);
        createTest("stealing", AnswerType.ANSWER_TYPE_IMAGE_URI, "A robber running away with money he stole from a bank", TestType.TEST_TYPE_SCENARIO);
    }

    public void SetAllTestApprovedStateToFalse()
    {
        List<Test> tests = Test.findWithQuery(Test.class, "SELECT * FROM TEST WHERE Approved = ? ", "1");

        for (Test test : tests)
        {
            test.setApproved(false);
            test.save();
        }
    }

    public void SetAllTestApprovedStateToFalseAsync()
    {
        new ChangeApprovedStateAsync().doInBackground();
    }

    private class ChangeApprovedStateAsync extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params)
        {
            List<Test> tests = Test.findWithQuery(Test.class, "SELECT * FROM TEST WHERE Approved = ? ", "1");

            for (Test test : tests)
            {
                test.setApproved(false);
                test.save();
            }

            return null;
        }
    }
}
