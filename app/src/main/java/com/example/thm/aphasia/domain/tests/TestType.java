package com.example.thm.aphasia.domain.tests;

/**
 * Created by Jeremy on 2/12/2016.
 */

public enum TestType
{


    TEST_TYPE_NONE("None"),
    TEST_TYPE_SCENARIO("Scenario"), // The patient is shown a sentence describing a scenario and has to pick the picture that best matches it.
    TEST_TYPE_SAY_OUT_LOUD("Speech Test"), // The patient is shown a picture (e.g. a candle) and is expected to say out loud what he sees (e.g. "candle!"). The app should record this and make it possible for researchers to easily access this data.
    TEST_TYPE_PICTURE_CHOOSE("Multiple Choice"); // The patient is shown a series of pictures (e.g. an ear, a hand, an arm and a finger). The app lets the patient hear a word (e.g. "hand"). The patient is then expected to pick the corresponding icon.

    private final String param;

    TestType(String string){
        param = string;
    }

    public String getStringRepresentation(){
        return param;

    }

}
