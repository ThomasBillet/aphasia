package com.example.thm.aphasia;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.example.thm.aphasia.domain.Result;
import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.answers.AnswerType;
import com.example.thm.aphasia.domain.tests.Test;
import com.example.thm.aphasia.domain.tests.TestType;
import com.example.thm.aphasia.io.QuestionsReaderParams;
import com.example.thm.aphasia.io.TestReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 28/12/2016.
 */

public class QuestionLoadActivity extends Activity
{
    private ProgressBar bar;
    private List<LeesVraagTask> tasks;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_load_layout);
        this.tasks = new ArrayList<>();
        this.handler = new Handler();
        this.bar = (ProgressBar)findViewById(R.id.progressBar);
        bar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.bar = (ProgressBar)findViewById(R.id.progressBar);
        scheduleNextTick();
    }

    private void scheduleNextTick()
    {
        this.handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onTick();
            }
        }, 500);
    }

    private void onTick()
    {
        if (!this.progress())
            scheduleNextTick();
    }

    public void start(View view)
    {
        // IN BACKGROUND
        this.read(Constants.SCENARIO_QUESTIONS_FILE, AnswerType.ANSWER_TYPE_TEXT, TestType.TEST_TYPE_SCENARIO);
        this.read(Constants.PICTURE_CHOOSE_QUESTIONS_FILE, AnswerType.ANSWER_TYPE_IMAGE_URI, TestType.TEST_TYPE_PICTURE_CHOOSE);
        this.read(Constants.SPEECH_QUESTIONS_FILE, AnswerType.ANSWER_TYPE_TEXT, TestType.TEST_TYPE_SAY_OUT_LOUD);
        scheduleNextTick();
    }

    private boolean progress()
    {
        if (checkDone())
            bar.setVisibility(View.INVISIBLE);
        else
            bar.setVisibility(View.VISIBLE);

        return checkDone();
    }

    public Boolean checkDone()
    {
        for (LeesVraagTask task : this.tasks)
            if (!task.getStatus().equals(AsyncTask.Status.FINISHED))
                return false;

        return true;
    }


    private void read(String filename, AnswerType answerType, TestType testType)
    {
        LeesVraagTask task = new LeesVraagTask();
        QuestionsReaderParams param = new QuestionsReaderParams(answerType,testType,filename,this);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,param);
        this.tasks.add(task);
    }

    public void flush(View view)
    {
        Test.deleteAll(Test.class);
        Answer.deleteAll(Answer.class);
        Result.deleteAll(Result.class);
    }

    private class LeesVraagTask extends AsyncTask<QuestionsReaderParams,Void,Void>
    {
        private TestReader reader;

        @Override
        protected Void doInBackground(QuestionsReaderParams... file)
        {
            QuestionsReaderParams param = file[0];
            this.reader = new TestReader(param.getFile(),param.getAnswerType(),param.getTestType());
            this.reader.lees(param.getContext());
            return null;
        }
    }
}
