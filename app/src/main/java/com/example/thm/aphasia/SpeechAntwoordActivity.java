package com.example.thm.aphasia;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.thm.aphasia.domain.Result;
import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.answers.AnswerType;
import com.example.thm.aphasia.domain.tests.Test;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Thomas on 19/11/2016.
 */

public class SpeechAntwoordActivity extends MainTestActivity
{
    private TextToSpeech speech;
    private ImageView iview;
    private SpeechRecognizer recog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.picture_say_layout);
        this.iview = (ImageView) findViewById(R.id.picture_say_imageview);
        this.recog = SpeechRecognizer.createSpeechRecognizer(this);
        super.onCreate(savedInstanceState);
    }

    public void decypherVoice(View view)
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, Constants.REQUEST_CODE_SPEECH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == Constants.REQUEST_CODE_SPEECH && resultCode == RESULT_OK)
        {
            List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            Test currentTest = getCurrentQuestion();
            String spokenText = "";

            if (results.contains(currentTest.getAnswer().getAnswer()))
                spokenText = currentTest.getAnswer().getAnswer();
            else if (results.size() > 0)
                spokenText = results.get(0);
            else
                spokenText = "ERROR_GETTING_TEXT";

            addTestResult(currentTest, new Answer(spokenText, AnswerType.ANSWER_TYPE_TEXT));

            nextQuestion();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showInList()
    {
        Picasso.with(this)
                .load("file://" + this.getExternalCacheDir().getPath() + Constants.PICTURES_PATH_FROM_ROOT + getCurrentQuestion().getQuestion() + Constants.PICTURES_EXTENSION)
                .into(iview);
    }
}
