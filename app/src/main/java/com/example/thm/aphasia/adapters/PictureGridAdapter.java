package com.example.thm.aphasia.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.thm.aphasia.domain.answers.Answer;

import java.util.List;

/**
 * Created by Thomas on 4/12/2016.
 */
// NIEUWE ADAPTER
public class PictureGridAdapter extends BaseAdapter {


    private Context context;
    private List<Answer> answers;


    protected Context getContext() {
        return context;
    }

    protected List<Answer> getAnswers() {
        return answers;
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    public PictureGridAdapter(Context context, List<Answer> answers) {

        this.context = context;
        this.answers = answers;


    }

    @Override
    public Object getItem(int position) {
        return answers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return answers.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        Answer a = (Answer)this.getItem(position);
        if (convertView == null) {

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();

            imageView = new ImageView(context);
            int screenWidth;

                screenWidth = metrics.widthPixels;
                imageView.setLayoutParams(new GridView.LayoutParams(screenWidth / 2, screenWidth / 2));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);



            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        Resources resources = context.getResources();
        final int resId = resources.getIdentifier(a.getAnswer(), "drawable", context.getPackageName());
        imageView.setImageDrawable(resources.getDrawable(resId));
        return imageView;
    }




}

