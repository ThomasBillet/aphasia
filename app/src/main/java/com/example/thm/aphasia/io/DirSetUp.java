package com.example.thm.aphasia.io;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.thm.aphasia.Constants;
import com.example.thm.aphasia.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Jeremy on 1/22/2017.
 */

public final class DirSetUp
{
    /*
     * Creates basic files in external cache with dir name "aphasiadirectory"
     * If this dir does not exists it will be created AND pictures will be initialised with resources.
     */
    public static File SetUpDirs(Context context, boolean convertResourcesToFiles)
    {
        File root = new File(context.getExternalCacheDir(), Constants.ROOT_DIR_NAME);
        if (!root.exists())
            root.mkdirs();

        File pictures = new File(root, Constants.PICTURES_DIR_NAME);
        if (!pictures.exists())
        {
            pictures.mkdir();
            if (convertResourcesToFiles)
            {
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.arrow, "arrow.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.book, "book.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.can, "can.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.candle, "candle.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.car, "car.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.cat, "cat.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.computer, "computer.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.cow, "cow.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.dog, "dog.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.glasses, "glasses.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.gorilla, "gorilla.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.horse, "horse.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.keyboard, "keyboard.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.lamp, "lamp.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.mouse, "mouse.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.pen, "pen.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.table, "table.png", pictures);

                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.dancing, "dancing.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.eating, "eating.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.fighting, "fighting.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.fishing, "fishing.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.guitar, "guitar.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.playing_footbal, "playing_footbal.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.showering, "showering.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.sleeping, "sleeping.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.stealing, "stealing.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.swimming, "swimming.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.tennis, "tennis.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.thinking, "thinking.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.watching_tv, "watching_tv.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.working, "working.png", pictures);
                createBitMapFromDrawableAndSave(context.getResources(), R.drawable.arresting, "arresting.png", pictures);
            }
        }

        return root;
    }

    private static void createBitMapFromDrawableAndSave(Resources res, int resId, String fileName, File dir)
    {
        Bitmap bm = BitmapFactory.decodeResource(res, resId);
        saveBitmapToFile(dir, fileName, bm, Bitmap.CompressFormat.PNG, 100);
    }

    /*
     * Bitmap.CompressFormat can be PNG,JPEG or WEBP.
     *
     * quality goes from 1 to 100. (Percentage).
     *
     * dir you can get from many places like Environment.getExternalStorageDirectory() or mContext.getFilesDir()
     * depending on where you want to save the image.
     */
    private static boolean saveBitmapToFile(File dir, String fileName, Bitmap bm, Bitmap.CompressFormat format, int quality)
    {
        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e)
        {
            Log.e("app",e.getMessage());
            if (fos != null)
            {
                try
                {
                    fos.close();
                } catch (IOException e1)
                {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }
}
