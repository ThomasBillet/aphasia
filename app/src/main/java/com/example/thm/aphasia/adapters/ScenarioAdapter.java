package com.example.thm.aphasia.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thm.aphasia.R;
import com.example.thm.aphasia.domain.answers.Answer;

import java.util.List;

/**
 * Created by Thomas on 5/12/2016.
 */

public class ScenarioAdapter extends ArrayAdapter<Answer> {
    public ScenarioAdapter(Context context, int resource, List<Answer> objects) {
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.scenario_list_item, null);
        }

        Answer res = this.getItem(position);
        if (res != null) {

            TextView scenario = (TextView) v.findViewById(R.id.scenarioveld);
            scenario.setText(res.getAnswer());


        }
        return v;
    }


}
