package com.example.thm.aphasia.domain.tests;

import com.example.thm.aphasia.domain.answers.Answer;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Thomas on 12/11/2016.
 */

public class Test extends SugarRecord
{
    private boolean approved;
    private Answer correctAnswer;
    private TestType type;
    private String question; // Scenario; Word (picture choose); image (uri) Say Out Loud;

    @Ignore
    private List<Answer> possibleAnswers;

    public Test()
    {
        approved = false;
        correctAnswer = null;
        type = TestType.TEST_TYPE_NONE;
        question = "";
        possibleAnswers = new ArrayList<>();
    }

    public Test(boolean approved, Answer answer, TestType type, String question)
    {
        this.approved = approved;
        this.correctAnswer = answer;
        this.type = type;
        this.question = question;
        possibleAnswers = new ArrayList<>();
    }

    public Answer getAnswer() {
        return correctAnswer;
    }

    public void setAnswer(Answer answer) {
        this.correctAnswer = answer;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getQuestion()
    {
        return question;
    }

    public Boolean isRightAnswer(Answer answer)
    {
        return correctAnswer.equals(answer);
    }

    public void initOptions()
    {
        switch (type)
        {
            case TEST_TYPE_SAY_OUT_LOUD:
                possibleAnswers =  new ArrayList<>(0);
                break;
            case TEST_TYPE_PICTURE_CHOOSE:
            case TEST_TYPE_SCENARIO:
                List<Answer> answers = Answer.findWithQuery(Answer.class, "SELECT DISTINCT * FROM ANSWER WHERE Id NOT IN (?) AND Type = ? ORDER BY RANDOM() LIMIT 3", correctAnswer.getId().toString(), correctAnswer.getType().toString());
                answers.add(correctAnswer);
                Collections.shuffle(answers);
                possibleAnswers = answers;
                break;
            default:
                break;
        }
    }

    public List<Answer> giveOptions()
    {
        return possibleAnswers;
    }

    public TestType getTestType()
    {
        return this.type;
    }
}
