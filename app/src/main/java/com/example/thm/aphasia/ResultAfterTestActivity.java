package com.example.thm.aphasia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.example.thm.aphasia.domain.Result;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Thomas on 27/12/2016.
 */

public class ResultAfterTestActivity extends Activity
{
    private TextView viewText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.resultaat_after_test_layout);
        this.viewText = (TextView) findViewById(R.id.resultaat_after_info);
    }

    private Result getResult()
    {
        Result result = Result.findWithQuery(Result.class,"SELECT * FROM RESULT ORDER BY id DESC LIMIT 1").get(0).initTestsResults();
        return result;
    }

    private void setText(Result result)
    {
        double amountright =  result.calculateScore();
        double lengte = result.getLength();

        double totalscore = (amountright / lengte) * 100;

        if(totalscore  <= 60)
            this.viewText.setText(getString(R.string.resultaat_60)+ " " + round(totalscore, 2)  + "%");
        else if(totalscore  <= 80)
            this.viewText.setText(getString(R.string.resultaat_80)+ " " + round(totalscore, 2)  + "%");
        else if(totalscore  >= 90)
            this.viewText.setText(getString(R.string.resultaat_90)+ " " + round(totalscore, 2)  + "%");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.setText(getResult());
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("text",viewText.getText().toString()).commit();
    }

    @Override
    public void finish()
    {
        Intent i = new Intent();
        setResult(RESULT_OK);

        super.finish();
    }

    public void okclicked(View view)
    {
        this.finish();
    }

    private double round(double value, int places)
    {
        if (places < 0)
            throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}




