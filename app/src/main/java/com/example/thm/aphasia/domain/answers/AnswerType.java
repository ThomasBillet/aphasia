package com.example.thm.aphasia.domain.answers;

/**
 * Created by Jeremy on 2/12/2016.
 */

public enum AnswerType
{
    ANSWER_TYPE_NONE,
    ANSWER_TYPE_TEXT,
    ANSWER_TYPE_IMAGE_URI
}
