package com.example.thm.aphasia.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.thm.aphasia.Constants;
import com.example.thm.aphasia.domain.answers.Answer;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Thomas on 30/12/2016.
 */

public class UrlPictureGridAdapter extends PictureGridAdapter
{
    public UrlPictureGridAdapter(Context context, List<Answer> answers)
    {
        super(context, answers);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ImageView imageView;
        Answer a = (Answer)this.getItem(position);
        if (convertView == null)
        {
            DisplayMetrics metrics = this.getContext().getResources().getDisplayMetrics();
            imageView = new ImageView(this.getContext());
            int screenWidth;
            screenWidth = metrics.widthPixels;
            imageView.setLayoutParams(new GridView.LayoutParams(screenWidth / 2, screenWidth / 2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);

        } else
        {
            imageView = (ImageView) convertView;
        }

        Picasso.with(this.getContext())
                .load("file://" + getContext().getExternalCacheDir().getPath() + Constants.PICTURES_PATH_FROM_ROOT + a.getAnswer() + Constants.PICTURES_EXTENSION)
                .into(imageView);

        return imageView;
    }
}
