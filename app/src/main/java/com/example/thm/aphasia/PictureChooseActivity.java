package com.example.thm.aphasia;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.thm.aphasia.adapters.UrlPictureGridAdapter;
import com.example.thm.aphasia.domain.answers.Answer;

import java.util.Locale;

/**
 * Created by Thomas on 12/11/2016.
 */

public class PictureChooseActivity extends MainTestActivity
{
    private TextToSpeech speech;
    private GridView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_picturespeechchooser);
        this.listview = (GridView)findViewById(R.id.answerlijst);
        this.textToSpeechInit();
        super.onCreate(savedInstanceState);
    }

    private void textToSpeechInit()
    {
        speech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener()
        {
            @Override
            public void onInit(int status)
            {
                if (status != TextToSpeech.ERROR)
                {
                    speech.setLanguage(Locale.getDefault());
                }
            }
        });
    }

    public void playSound(View v)
    {
        convertToSpeech(getCurrentQuestion().getQuestion());
    }

    private void convertToSpeech(String speech)
    {
        this.speech.speak(speech, TextToSpeech.QUEUE_ADD, null);
    }

    @Override
    public void showInList()
    {
        // MOET FINAL
        final UrlPictureGridAdapter adapter = new UrlPictureGridAdapter(this, getCurrentQuestion().giveOptions());

        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                addTestResult(getCurrentQuestion(), (Answer)adapter.getItem(position));
                nextQuestion();
            }
        });
    }
}
