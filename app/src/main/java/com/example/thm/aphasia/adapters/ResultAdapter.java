package com.example.thm.aphasia.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.thm.aphasia.R;
import com.example.thm.aphasia.domain.Result;

import java.util.List;

/**
 * Created by Thomas on 4/12/2016.
 */

public class ResultAdapter extends ArrayAdapter<Result> {
    public ResultAdapter(Context context, int resource, List<Result> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.result_layout_item_layout, null);
        }

        Result res = (Result) this.getItem(position);

        if(res != null) {
            TextView title = (TextView) v.findViewById(R.id.result_title_textview);
            TextView date = (TextView) v.findViewById(R.id.result_date_textview);
            TextView score = (TextView) v.findViewById(R.id.result_score_textview);


            title.setText(res.getTestTypeAsString());
            date.setText(res.getDate().toString());
            score.setText(res.calculateScore() + " / " + res.getLength());


        }

        return v;
    }

}
