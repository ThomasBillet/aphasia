package com.example.thm.aphasia.domain.answers;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Thomas on 13/11/2016.
 */

public class Answer extends SugarRecord
{
    private String answer; // woord, zin, naam van afbeelding in resources
    private AnswerType type;

    public Answer()
    {
        answer = "";
        type = AnswerType.ANSWER_TYPE_NONE;
    }

    public Answer(String answer, AnswerType type)
    {
        this.answer = answer;
        this.type = type;
    }

    public String getAnswer()
    {
        return answer;
    }

    public void setAnswer(String answer)
    {
        this.answer = answer;
    }

    public AnswerType getType()
    {
        return type;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof Answer)
            return ((Answer)o).getAnswer().equals(answer);

        return false;
    }
}
