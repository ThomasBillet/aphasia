package com.example.thm.aphasia.io;

import android.content.Context;

import com.example.thm.aphasia.domain.answers.AnswerType;
import com.example.thm.aphasia.domain.tests.TestType;

/**
 * Created by Thomas on 28/12/2016.
 */

public class QuestionsReaderParams {

    private AnswerType answerType;
    private TestType testType;
    private String file;
    private Context context;

    public QuestionsReaderParams(AnswerType answerType, TestType testType, String file, Context context) {
        this.answerType = answerType;
        this.testType = testType;
        this.file = file;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
