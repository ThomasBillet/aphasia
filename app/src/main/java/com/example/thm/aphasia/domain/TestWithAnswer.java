package com.example.thm.aphasia.domain;

import com.example.thm.aphasia.domain.answers.Answer;
import com.example.thm.aphasia.domain.tests.Test;

/**
 * Created by Jeremy on 28/11/2016.
 */


//TUPLE !!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class TestWithAnswer
{
    private Test test;
    private Answer givenAnswer;

    public Test getTest() {
        return test;
    }

    private void setTest(Test test) {
        this.test = test;
    }

    public Answer getGivenAnswer() {
        return givenAnswer;
    }

    private void setGivenAnswer(Answer givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public TestWithAnswer(Test test, Answer givenAnswer)
    {
        this.test = test;
        this.givenAnswer = givenAnswer;
    }

    public boolean isCorrect()
    {
        return test.isRightAnswer(givenAnswer);
    }
}
